# Proto POC

## Credit Abuse Proto

The `credit_abuse/` folder contains:
- v0 of the Credit Abuse ML Model Proto
- An example client (Python)
- An example server (Python)
- Rudimentary sample data that can be used for testing
  - Note: More representative sample data to come

### Development

1. Set up developer environment

```sh
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install --upgrade pip
$ pip install -r requirements.txt
```

2. Generate proto stubs

```sh
$ start_example_server
```

3. Execute an example request against the above server

```sh
# in a separate terminal
$ python credit_abuse/example/credit_abuse_client.py
```

4. (Optional) Clean Up

   - this will delete  all existing gRPC stubs within the repo

```sh
$ make clean
```
