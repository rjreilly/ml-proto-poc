help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

clean: ## Deleted unwanted files and folders
	echo "Cleaning make_tabular_data"
	find . -name "credit_abuse_pb2*" -type f -delete


generate_stubs: ## Generates Credit Abuse gRPC Stubs
	python -m grpc_tools.protoc -I credit_abuse/proto --python_out=credit_abuse/example --grpc_python_out=credit_abuse/example  credit_abuse/proto/credit_abuse.proto


start_example_server: ## Starts an example Credit Abuse Model Server (Python)
	python credit_abuse/example/credit_abuse_server.py


example_request: ## Executes an example RequestCreditAbuseModelScore against the local test server
	python credit_abuse/example/credit_abuse_client.py
