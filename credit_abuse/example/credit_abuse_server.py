"""This server represents what will be the Sagemaker endpoint hosting the credit abuse model"""

from concurrent import futures
import logging

import grpc

import credit_abuse_pb2
import credit_abuse_pb2_grpc


class CreditAbuseModel(credit_abuse_pb2_grpc.CreditAbuseModelServicer):
    def RequestCreditAbuseModelScore(self, request, context):
        return credit_abuse_pb2.CreditAbuseScoreResponse(
            score=0.03,
            version="0.0.0",
            error=None,
            reasons={
                "reason_1": "first reason",
                "reason_2": "second reason",
                "reason_3": "third reason",
            },
        )


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    credit_abuse_pb2_grpc.add_CreditAbuseModelServicer_to_server(
        CreditAbuseModel(), server
    )
    server.add_insecure_port("[::]:50051")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    logging.basicConfig()
    print("starting server")
    serve()
