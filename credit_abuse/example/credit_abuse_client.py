import logging

import grpc

import credit_abuse_pb2
import credit_abuse_pb2_grpc


def request_credit_abuse_score(score_request: credit_abuse_pb2.CreditAbuseScoreRequest):
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = credit_abuse_pb2_grpc.CreditAbuseModelStub(channel)
        response = stub.RequestCreditAbuseModelScore(score_request)

        response_dict = {
            "score": response.score,
            "version": response.version,
            "error": response.error,
            "reasons": response.reasons,
        }

    return response_dict


if __name__ == "__main__":
    import csv

    # load sample data into python dict
    with open("credit_abuse/sample_data/sample_data.csv", mode="r") as data:
        header = []
        feature_vectors = []
        for row in csv.reader(data):
            if not header:
                header = row
            else:
                row_dict = {}
                for i, value in enumerate(row):
                    row_dict[header[i]] = int(value)

                feature_vectors.append(row_dict)

    for feature_vector in feature_vectors:
        response = request_credit_abuse_score(
            credit_abuse_pb2.CreditAbuseScoreRequest(**feature_vector)
        )

        print(response)
